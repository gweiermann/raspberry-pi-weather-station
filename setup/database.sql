
create database weather_station;
use weather_station;

grant all privileges on *.* to 'weather_station'@'localhost' identified by '123abc';

create table Measurement (
  id int not null auto_increment primary key,
  temperature float not null,
  humiture float not null,
  isRaining boolean not null,
  time timestamp default now()
);

insert into Measurement (id, temperature, humiture, isRaining) values (1, 0, 0, false);