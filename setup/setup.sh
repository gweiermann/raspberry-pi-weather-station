#!/bin/sh


log() {
    while read text; do
        echo $text | sudo tee -a /tmp/weather_station_setup.log
    done;
}

if [ -e /tmp/weather_station_setup.log ]
then
  sudo rm /tmp/weather_station_setup.log
fi

echo "\n\n"

echo "Log of Setup:" | log > /dev/null

echo "*** For errors, please read the /tmp/weather_station_setup.log file ***"
echo | log
echo "> Installing required packages. Operations will take a long time" | log
echo "Updating..." | log
sudo apt-get update | log > /dev/null
echo "Installing Dependencies..." | log
sudo apt-get --assume-yes install php mysql-server php7.0-mysql ntpdate build-essential python-dev python-openssl git insserv | log > /dev/null

echo | log
echo "> Getting Weather-Station Source..." | log
cd /var/www
echo "Pulling Weather-Station..." | log
sudo git clone https://bitbucket.org/gweiermann/raspberry-pi-weather-station.git -q weather-station | log > /dev/null
cd weather-station

echo | log
echo "> Setting up Adafruit_DHT..." | log
echo "Pulling Adafruit_DHT..."
sudo git clone https://github.com/adafruit/Adafruit_Python_DHT.git -q | log > /dev/null
echo "Installing Adafruit_DHT..." | log
cd Adafruit_Python_DHT
sudo python setup.py install | log > /dev/null
cd ..


echo | log
echo "> Creating Database..." | log
cat ./setup/database.sql | sudo mysql -uroot | log > /dev/null

echo | log
echo "> Setting up Apache-Server..." | log
sudo mkdir logs | log
sudo cp ./setup/virtualhost.conf /etc/apache2/sites-available/weather-station.conf | log
printf '.'
cd htdocs
sudo wget -q https://github.com/chartjs/Chart.js/releases/download/v2.7.3/Chart.bundle.min.js | log > /dev/null
cd ..
printf '.'
sudo a2ensite weather-station.conf | log > /dev/null
printf '.'
sudo a2dissite 000-default.conf | log > /dev/null
echo '.'
sudo systemctl reload apache2 | log > /dev/null

echo
echo "> Giving permissions..." | log
sudo chmod g+s /var/www
sudo chmod 775 /var/www
sudo chown -R www-data:www-data /var/www

echo
echo "> Changing hostname..." | log
echo "weather-station" | sudo tee /etc/hostname  | log > /dev/null
echo "appending to /etc/hosts" | log > /dev/null
echo "127.0.1.1    weather-station" | sudo tee -a /etc/hosts | log > /dev/null

echo
echo "> Enable reading sensors at startup..." | log
sudo cp ./setup/autostart.sh /etc/init.d/weather-station | log > /dev/null
printf '.'
sudo chmod a+x /etc/init.d/weather-station | log > /dev/null
sudo update-rc.d weather-station defaults | log > /dev/null
echo '.'
sudo insserv /etc/init.d/weather-station | log > /dev/null

echo
echo "> Removing the trashy setup folder..." | log
sudo rm -rf ./setup | log > /dev/null

echo
echo "Finished!"
echo "Please reboot the system (sudo reboot)";
