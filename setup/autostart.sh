#!/bin/sh
### BEGIN INIT INFO
# Provides: weather-station
# Required-Start: $remote_fs $syslog
# Required-Stop: $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Updates the weather station data
# Description: Updates the weather station database every 5 minutes
### END INIT INFO

start() {
  cd /var/www/weather-station
  sudo python interval.py
}

case "$1" in
    start)
        start
        ;;
    stop)
        exit 1
        ;;
    restart)
        exit 1
        ;;
    *)
        start
        ;;
esac

exit 0
