<?php

header("Access-Control-Allow-Origin: *");

require_once("../lib.php");

header("Content-Type: application/json");

$mode = $_GET['mode'];

$result = [];
if ($mode === 'lasts') {
    $count = intval( $_GET['count'] );
    $measurements = Storage::getLastMeasurements($count);
    foreach ($measurements as $measurement) {
        array_push($result, $measurement->serialize());
    }
}
else if ($mode === 'last') {
    $result = Storage::getLastMeasurement()->serialize();
} else if ($mode === 'day') {
    $date = $_GET['date'];
    $measurements = Storage::getMeasurementsOfDay( intval($date) );
    foreach ($measurements as $measurement) {
        array_push($result, $measurement->serialize());
    }
} else if ($mode === 'days') {
    $from = $_GET['from'];
    $to = $_GET['to'];
    $measurements = Storage::getDayMeasurements( intval($from), intval($to) );
    foreach ($measurements as $measurement) {
        array_push($result, $measurement->serialize());
    }
} else {
    $result = "please specify a correct mode!";
}

echo json_encode($result);