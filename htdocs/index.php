<?php require_once("../lib.php"); ?>
<!DOCTYPE html>

<html>

    <head>
        <title>Wetter Station</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    </head>

    <body>

        <ul class="container-list">
            <li class="container">
                <li class="container" id="image">
                    <img src="" width="80" height="80" />
                </li>
                <li class="container" id="temperature">
                    <span class="value">
                        <?php echo Station::$measurement->getTemperature(); ?>
                    </span>
                    <span class="unit">°C</span>
                </li>
            </li>
            <li class="container" id="humiture">
                <span class="image">
                    <img src="./images/drop.svg" width="50" height="20" />
                </span>
                <span class="value">
                    <?php echo Station::$measurement->getHumiture(); ?>
                </span>
                <span class="unit">%</span>
            </li>
            <li class="container" id="raining">
                <span class="value <?php echo Station::$measurement->isRaining() ? "raining" : "notRaining"; ?>">
                    Es regnet
                </span>
            </li>
            <li class="container" id="chartcontainer">
              <div class="header">
                Statistiken
              </div>
              <canvas id="chartDay"></canvas>
              <canvas id="chartDays"></canvas>
            </li>
        </ul>

        <script src="Chart.bundle.min.js"></script>
        <script src="script.js"></script>
    </body>

</html>
