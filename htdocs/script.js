
let elementImage = document.querySelector("#image > img");
let elementTemperatureValue = document.querySelector("#temperature > .value");
let elementHumitureValue = document.querySelector("#humiture > .value");
let elementRainingValue = document.querySelector("#raining > .value");

function failedUpdatingData(e) {
    console.error("Failed updating data!", e);
}

function updateData() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "./api.php?mode=last");
    xhr.onload = function() {
        if (xhr.status >= 200 && xhr.status < 300) {
            try {
                let json = JSON.parse(xhr.responseText);
                // loaded data, now update
                elementTemperatureValue.innerHTML = json.temperature;
                elementHumitureValue.innerHTML = json.humiture;
                elementRainingValue.classList.remove(!json.isRaining ? "raining" : "notRaining");
                elementRainingValue.classList.add(json.isRaining ? "raining" : "notRaining");
                updateImage();
            } catch(e) {
                failedUpdatingData(e);
            }
        } else {
            failedUpdatingData();
        }
    };
    xhr.send();
}

function updateImage() {
    let isRaining = elementRainingValue.classList.contains("raining");
    let temperature = parseFloat(elementTemperatureValue.innerHTML);
    let src = "";
    if (isRaining) {
        if (temperature >= 20) {
            src = "rainbow.svg";
        } else {
            src = "rain.svg";
        }
    } else {
        if (temperature > 0) {
            if (temperature >= 15) {
                src = "sun.svg";
            } else {
                src = "cloud.svg";
            }
        } else {
            src = "snow.svg";
        }
    }
    elementImage.src = "./images/" + src;
}


updateImage();

// update data every 5min
setInterval(updateData, 1000 * 60 * 5);





// the chart
let dataDay = {
  labels: [],
  datasets: [
      {
          label: 'Luftfeuchtigkeit',
          yAxisID: 'humiture',
          backgroundColor: "rgba(100, 100, 255, .1)",
          data: []
      },
      {
          label: 'Temperatur',
          yAxisID: 'temperature',
          backgroundColor: "rgba(255, 100, 100, .05)",
          data: []
      }
  ]
};

let optionsDay = {
  responsive: true,
  //aspectRation: 10,
  scales: {
      yAxes: [
          {
              id: 'humiture',
              position: 'right',
              ticks: {
                  suggestedMin: 0,
                  suggestedMax: 100,
                  beginAtZero: true,
                  userCallback: (item) => `${item} %`
              }
          },
          {
              id: 'temperature',
              ticks: {
                  suggestedMin: 0,
                  suggestedMax: 40,
                  beginAtZero: true,
                  userCallback: (item) => `${item} °C`
              }
          }
      ]
  },
  tooltips: {
      enabled: true,
      callbacks: {
          label: (items, data) => items.datasetIndex === 0 ? `Luftfeuchtigkeit: ${items.yLabel} %` : `Temperatur: ${items.yLabel} °C`
      }
  }
}

let dataDays = {
  labels: [],
  datasets: [
      {
          label: 'Luftfeuchtigkeit',
          yAxisID: 'humiture',
          backgroundColor: "rgba(100, 100, 255, .1)",
          lineTension: 0,
          data: []
      },
      {
          label: 'Höchste Temperatur',
          yAxisID: 'temperature',
          backgroundColor: "rgba(255, 100, 100, .05)",
          color: "rgba(255, 100, 100, 1)",
          lineTension: 0,
          data: []
      },
      {
          label: 'Niedrigste Temperatur',
          yAxisID: 'temperature',
          backgroundColor: "rgba(255, 100, 100, .05)",
          lineTension: 0,
          data: []
      }
  ]
};

let optionsDays = {
  responsive: true,
  scales: {
      yAxes: [
          {
              id: 'humiture',
              position: 'right',
              ticks: {
                  suggestedMin: 0,
                  suggestedMax: 100,
                  beginAtZero: true,
                  userCallback: item => `${item} %`
              }
          },
          {
              id: 'temperature',
              ticks: {
                  suggestedMin: 0,
                  suggestedMax: 40,
                  beginAtZero: true,
                  userCallback: item => `${item} °C`
              }
          }
      ]
  },
  tooltips: {
      enabled: true,
      callbacks: {
          label: (items, data) => ([`Luftfeuchtigkeit: ${items.yLabel} %`, `Niedrigste Temperatur: ${items.yLabel} °C`, `Höchste Temperatur: ${items.yLabel} °C`])[items.datasetIndex]
      }
  },
  elements: {
      line: {
          tension: 0
      }
  }
}


let canvasDay = document.getElementById("chartDay");
let ctxDay = canvasDay.getContext("2d");
let canvasDays = document.getElementById("chartDays");
let ctxDays = canvasDays.getContext("2d");
let statisticsElement = document.getElementById("chartcontainer");

let chartDay = new Chart(ctxDay, {
  type: 'line',
  data: dataDay,
  options: optionsDay
});

let chartDays = new Chart(ctxDays, {
  type: 'line',
  data: dataDays,
  options: optionsDays
});


let timeRegex = /(\d{4})-(\d{2})-(\d{2})(?: (\d{2}):(\d{2}):(\d{2}))?/;

async function requestAndUpdateDay() {
  let day = Math.floor( Date.now() / 1000 );
  let xhr = new XMLHttpRequest();
  let json = await (await fetch(`./api.php?mode=lasts&count=${6}`)).json();
  json = json.reverse();
  let times = json.map( (item) => {
      let match = item.time.match(timeRegex);
      return `${match[4]}:${match[5]}`;
  });
  let temperatures = json.map( item => Math.round( item.temperature * 100 ) / 100 );
  let humitures    = json.map( item => Math.round( item.humiture    * 100 ) / 100 );
  chartDay.data.labels = times;
  chartDay.data.datasets[0].data = humitures;
  chartDay.data.datasets[1].data = temperatures;
  chartDay.config.options.scales.yAxes[1].ticks.suggestedMax = Math.max(...temperatures) + 3;
  if (!json.length) {
    canvasDay.style.display = "none";
  } else {
    canvasDay.style.display = "block";
    chartDay.update();
  }
}

async function requestAndUpdateDays(dayCount = 5) {
  let from = Math.floor( Date.now() / 1000 );
  let to   = Math.floor( Date.now() / 1000 - dayCount * 60 * 60 * 24);
  let json = await (await fetch(`./api.php?mode=days&from=${from}&to=${to}`)).json();
  json = json.reverse();
  let days = json.map( item => {
      let match = item.date.match(timeRegex);
      return `${match[3]}.${match[2]}`;
  });
  let lowestTemperatures  = json.map( item => Math.round( item.minimumTemperature * 100 ) / 100 );
  let highestTemperatures = json.map( item => Math.round( item.maximumTemperature * 100 ) / 100 );
  let humitures = json.map( item =>  Math.round( item.averageHumiture * 100 ) / 100 );
  chartDays.data.labels = days;
  chartDays.data.datasets[0].data = humitures;
  chartDays.data.datasets[1].data = lowestTemperatures;
  chartDays.data.datasets[2].data = highestTemperatures;
  chartDays.config.options.scales.yAxes[1].ticks.suggestedMax = Math.max(...highestTemperatures) + 3;
  if (!json.length) {
    canvasDays.style.display = "none";
  } else {
    canvasDays.style.display = "block";
    chartDays.update();
  }
}

async function requestAndUpdateData() {
  await requestAndUpdateDay();
  await requestAndUpdateDays();
  if (!chartDay.data.datasets[0].data.length && !chartDays.data.datasets[0].data.length) {
    statisticsElement.style.display = "none";
  } else {
    statisticsElement.style.display = "block";
  }
}

requestAndUpdateData();
setInterval(requestAndUpdateData, 1000 * 60 * 60); // 1h
