## Was ist das?
Eine kleine Wetterstation, die die Temperatur und Luftfeuchtigkeit misst, wie auch Regen registriert und sammelt. 
Diese Daten werden zusammengefasst und anschaulich auf einem Apache-Webserver angezeigt.

## Installation
Ein DHT11 Sensor muss an Pin GPIO 4 angeschlossen werden, sowie ein Regensensor an GPIO 7.
Nach aufsetzen eines Raspberry Pis kann ist nur dieser Befehl hier nötig. Der Rest passiert automatisch:  
```curl https://bitbucket.org/gweiermann/raspberry-pi-weather-station/raw/master/setup/setup.sh | sudo /bin/sh```

## Fertig
Nach dem Neustarten ist die Wetterstation schon einsatzbereit und kann über ihre IP abgerufen werden  

## Konfigurationen
Gehe zu erst einmal in den Ordner /var/www/weather-station/. Dies tust du mit  
```cd /var/www/weather-station```  
Als nächstes kannst du in der Datei reader.py die oben genannten Pins auch umstellen.  
In der Datei interval.py kannst du die Update-Zeiten ändern.  
Standardmäßig ist das sonst so:  
 * Live-Wetterdaten aktuallisieren sich alle 5 Minuten  
 * Statistik-Wetterdaten aktuallisieren sich alle 4 Stunden
  
Um Dateien zu öffnen musst du folgenden Befehl eingeben:  
```sudo nano dateiname```  
Beispiel:  
```sudo nano reader.py```  
Du kannst außerdem die Wetterdaten manuell aktualliseren.  
Um die Live-Daten zu aktuallisieren muss dieser Befehl erfolgen:  
```php updateLive.php```  
Um die Statistiken zu updaten musst du den letzten Schritt befolgen und diesen Befehl eingeben:  
```php update.php``` 

## Schnittstelle
Die Webseite bietet außerdem eine API an, die folgendermaßen benutzt werden muss.  
Hinweis: Daten kommen immer mittels JSON zurück und so werden <Parameter> gekennzeichnet, die mit entsprechenden Werten   ersetzt werden müssen. Außerdem ist die Adresse der Seite hier einfach *wetterstation.de*.  
#### Bekomme den Live Datenwert
```wetterstation.de/api.php?mode=last```  
#### Bekomme die letzten Datensätze der Statistik
```wetterstation.de/api.php?mode=lasts&count=<Anzahl>```  
#### Bekomme die Datensätze eines ganzen Tages
```wetterstation.de/api.php?mode=day&date=<Datum>```  
Datum ist der UNIX-Timestamp in Sekunden  
#### Bekomme die kleinste und größte Temperaturen von mehreren Tagen
```wetterstation.de/api.php?mode=days&from=<Von-Wann>&to=<Bis-Wann>```  
Jeweils auch als UNIX-Timestamp in Sekunden angeben  

## Editieren
Fühl dich frei hier alles zu editieren. Wenn du etwas verbesserst kannst du auch gerne einen Pull-Request starten.  

## Author
Gerrit Weiermann