#!/usr/bin/env python
import sys
import Adafruit_DHT
import RPi.GPIO as GPIO

pin = 4
rainPin = 17
sensor = Adafruit_DHT.DHT11

GPIO.setmode(GPIO.BCM)
GPIO.setup(rainPin, GPIO.IN)

humiture, temperature = Adafruit_DHT.read_retry(sensor, pin)
if GPIO.input(rainPin):
    isRaining = "true"
else:
    isRaining = "false"

if humiture is not None and temperature is not None:
    print("{\"temperature\": " + str(temperature) + ", \"humiture\": "  + str(humiture) + ", \"isRaining\": " + isRaining + "}")
else:
    print("fail")
