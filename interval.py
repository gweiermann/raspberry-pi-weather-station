import time
import subprocess

# your settings
updateInterval = 60 * 5    # seconds
saveInterval   = 60 * 60 * 4 # seconds

# program

def getNextTime(interval):
    return (time.time() // interval) * interval + interval

def getTimeLeft():
    return next - time.time()

def updateStorage():
    subprocess.call(["php", "update.php"])

def updateLive():
    subprocess.call(["php", "updateLive.php"])

next = getNextTime(updateInterval)
saveNext = getNextTime(saveInterval)

# the loop di doop
while (True):
    timeleft = getTimeLeft()
    if (timeleft <= 0):
        if next >= saveNext:
            updateStorage()
            saveNext = getNextTime(saveInterval)
        next = getNextTime(updateInterval)
        updateLive()
