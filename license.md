# Licenses
Chart.js: MIT License
Apache: Apache2 License
PHP: PHP License v3.01
Python: PSF LICENSE AGREEMENT FOR PYTHON 3.7.2
Linux: GNU General Public License
