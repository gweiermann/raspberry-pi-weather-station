<?php

class Measurement {
  private $temperature = 0;
  private $humiture = 0;
  private $raining = false;
  private $time = null;

  public function __construct($temperature, $humiture, $isRaining, $time = null) {
    $this->temperature = $temperature;
    $this->humiture = $humiture;
    $this->raining = $isRaining;
    $this->time = $time ? $time : time();
  }

  public function getTemperature() {
    return $this->temperature;
  }
  public function getHumiture() {
    return $this->humiture;
  }
  public function getTime() {
    return $this->time;
  }
  public function isRaining() {
    return $this->raining;
  }

  public function setTemperature($temperature) {
    $this->temperature = $temperature;
    return $this;
  }

  public function setHumiture($humiture) {
    $this->humiture = $humiture;
    return $this;
  }

  public function setRaining($raining) {
    $this->raining = $raining;
    return $this;
  }

  public function setTime($time) {
    $this->time = $time;
    return $this;
  }

  public static function deserialize($obj) {
    if (array_key_exists("time", $obj)) {
      return new Measurement($obj["temperature"], $obj["humiture"], $obj["isRaining"] + 0, $obj["time"]);
    } else {
      return new Measurement($obj["temperature"], $obj["humiture"], $obj["isRaining"] + 0);
    }
  }

  public function serialize() {
    return [
      "temperature" => $this->getTemperature(),
      "humiture" => $this->getHumiture(),
      "isRaining" => $this->isRaining(),
      "time" => $this->getTime()
    ];
  }
}

class DayMeasurement {
  private $minimumTemperature;
  private $maximumTemperature;
  private $averageHumiture;
  private $date;
  public function __construct(float $minimumTemperature, float $maximumTemperature, float $averageHumiture, $date) {
    $this->minimumTemperature = $minimumTemperature;
    $this->maximumTemperature = $maximumTemperature;
    $this->averageHumiture = $averageHumiture;
    $this->date = $date;
  }
  public function getMinimumTemperature() {
    return $this->minimumTemperature;
  }
  public function getMaximumTemperature() {
    return $this->maximumTemperature;
  }
  public function getAverageHumiture() {
    return $this->averageHumiture;
  }
  public function getDate() {
    return $this->date;
  }

  public function serialize() {
    return [
      "minimumTemperature" => $this->getMinimumTemperature(),
      "maximumTemperature" => $this->getMaximumTemperature(),
      "averageHumiture" => $this->getAverageHumiture(),
      "date" => $this->getDate()
    ];
  }
  public function deserialize($data) {
    return new DayMeasurement(
      $data['minimumTemperature'],
      $data['maximumTemperature'],
      $data['averageHumiture'],
      $data['date']
    );
  }
}


class Database {
  private $username;
  private $password;
  private $dbname;
  private $connection;

  public function __construct($username, $password, $dbname) {
    $this->username = $username;
    $this->password = $password;
    $this->dbname = $dbname;
    $this->connect();
  }

  private function connect() {
    $this->connection = new PDO("mysql:host=localhost;dbname=" . $this->dbname, $this->username, $this->password);
    $this->connection->setATtribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $this;
  }

  public function get($query, $items = []) {
    $statement = $this->connection->prepare($query);
    $statement->execute($items);
    return $statement->fetchAll(PDO::FETCH_ASSOC);
  }

  public function run($query, $items = []) {
    $this->connection->prepare($query)->execute($items);
    return $this;
  }
}

class Storage {
  private static $database;

  public static function init($database) {
    self::$database = $database;
  }

  public static function getLastMeasurement() {
    $raw = self::$database->get("SELECT * FROM Measurement WHERE id=1");
    if ($raw === null || count($raw) === 0) {
      return null;
    }
    return Measurement::deserialize($raw[0]);
  }

  public static function getLastMeasurements(int $count = 1) {
    $raw = self::$database->get("SELECT * FROM Measurement WHERE id!=1 ORDER BY TIME DESC LIMIT $count");
    if ($raw === null || count($raw) === 0) {
      return null;
    }
    $result = [];
    foreach ($raw as $entry) {
      array_push($result, Measurement::deserialize($entry));
    }
    return $result;
  }

  public static function getMeasurementsOfDay(int $date) {
    $raw = self::$database->get("SELECT * FROM Measurement WHERE id!=1 AND DATE(time)=? ORDER BY time DESC", [date("Y-m-d", $date)]);
    $result = [];
    foreach ($raw as $entry) {
      array_push($result, Measurement::deserialize($entry));
    }
    return $result;
  }

  public static function getDayMeasurements(int $date_from, int $date_to) {
    $raw = self::$database->get("SELECT MIN(temperature) AS minimumTemperature, MAX(temperature) AS maximumTemperature, AVG(humiture) AS averageHumiture, isRaining, DATE(time) AS date
                                    FROM Measurement
                                    WHERE id!=1 AND ? > DATE(time) AND DATE(time) < ?
                                    GROUP BY DATE(time)
                                    ORDER BY time DESC", [ 
                                      date("Y-m-d", $date_from),
                                      date("Y-m-d", $date_to)
                                    ]);
    $result = [];
    foreach ($raw as $entry) {
      array_push($result, DayMeasurement::deserialize($entry));
    }
    return $result;
  }

  public static function updateLiveMeasurement($measurement) {
    self::$database->run("UPDATE Measurement SET temperature=?, humiture=?, isRaining=? WHERE id=1", [
      $measurement->getTemperature(),
      $measurement->getHumiture(),
      $measurement->isRaining()
    ]);
  }

  public static function saveMeasurement() {
    $measurement = self::getLastMeasurement();
    self::$database->run("INSERT INTO Measurement (temperature, humiture, isRaining) VALUES (?, ?, ?)", [
      $measurement->getTemperature(),
      $measurement->getHumiture(),
      $measurement->isRaining()
    ]);
  }
}

Storage::init( new Database("weather_station", "123abc", "weather_station") );


class Station {
  public static $measurement = null;

  public static function init() {
    self::$measurement = Storage::getLastMeasurement();
    if (self::$measurement === null) {
      self::readAndSave();
    }
  }

  public static function read() {
    $path = __DIR__;
    $result = shell_exec("python $path/reader.py"); // also not working
    if ($result !== "fail") {
      $result = json_decode($result, true);
      if ($result !== null) {
        return Measurement::deserialize($result);
      }
    }
    error_log("Failed reading sensor. Result of reader.py gave null: '$result'");
    return null;
  }

  public static function saveLiveMeasurementToStatistics() {
    Storage::saveMeasurement();
  }

  public static function readAndSaveLive() {
    $measurement = self::read();
    if ($measurement !== null) {
      Storage::updateLiveMeasurement($measurement);
      self::$measurement = $measurement;
    }
  }

  public static function getMeasurement() {
      return self::$measurement;
  }
}

Station::init();